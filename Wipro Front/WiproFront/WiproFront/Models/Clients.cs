﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace WiproFront.Models
{
    [Table("clients")]
    public class Clients
    {
        [Column("name")]
        public string Name { get; set; }
        [Column("name")]
        public string Cpf { get; set; }
        [Column("name")]
        public string Adress { get; set; }
    }
}
