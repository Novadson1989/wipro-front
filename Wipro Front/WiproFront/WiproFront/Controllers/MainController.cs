﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WiproFront.Models;

namespace WiproFront.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MainController : ControllerBase
    {
        [HttpGet]
        public Clients[] Get()
        {
            return new List<Clients>() {
                new Clients(){ Name="Novadson Thelus",  Cpf="00000000000",Adress="Morumbi,R.da Prata 887,Cascavel-PR"},
                new Clients(){  Name="Jana Silva",Cpf="11111111111",Adress="Centro,9080,Salvador-BA"},
                new Clients(){Name="Marina Jean" ,Cpf="222222222222",Adress="Centro,9806,Toledo-PR"},
                new Clients(){ Name="Sérgio Joseph",Cpf="09876543212",Adress="Rua São paulo,883,Cascavel-PR"},
                new Clients(){ Name="Rosi Rodrigues",Cpf="34567890432",Adress="Rua Logoa Bonita 996,Cascavel-PR" },
            }.ToArray();
        }

    }
}
