import { Component } from '@angular/core';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})

export class FetchDataComponent {
  public clients: Cliensts[];
  dados: any;
  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<Cliensts[]>(baseUrl + 'clients').subscribe(result => {
      this.clients = result;
    }, error => console.error(error));

    this.dados = [
      {
        "id": "1",
        "name": "Teste 1"
      },
      {
        "id": "2",
        "name": "Teste 2"
      },
      {
        "id": "3",
        "name": "Teste 2"
      }
    ]
  }
}

interface Cliensts {
  name: string;
  cpf: string;
  adress: string;
}
